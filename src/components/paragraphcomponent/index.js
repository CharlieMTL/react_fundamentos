import React from 'react';

const ParagraphComponent = ({title}) => {
    return (
        <div>
            <h1>{title}</h1>
        </div>
    );
};

export default ParagraphComponent;