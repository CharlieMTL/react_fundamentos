import React from 'react';

const Titlecomponent = ({title}) => {
    return (
        <div>
            <h1>{title}</h1> 
        </div>
    );
};

export default Titlecomponent;