import React, { Component } from 'react';

import {
    ParagraphComponent,
} from "../../components"

class MainContainer extends Component {
    render() {
        return (
            <ParagraphComponent
                title={'MainContainer'}
            />
        );
    }
}

export default MainContainer;