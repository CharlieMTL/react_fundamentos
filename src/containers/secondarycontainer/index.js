import React, { Component } from 'react';

import {
    ParagraphComponent,
} from "../../components"

class SecondaryContainer extends Component {
    render() {
        return (
            <ParagraphComponent
                title={'SecondaryContainer'}
            />
        );
    }
}

export default SecondaryContainer;